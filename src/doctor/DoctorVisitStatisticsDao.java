
package doctor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

public class DoctorVisitStatisticsDao {

	private Connection cn;

	public void init() {
		try {
			if (cn != null && !cn.isClosed()) {
				return;
			}
			cn = DriverManager.getConnection("jdbc:hsqldb:file:db", "SA", "");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		String sqlCreateHeader = "CREATE TABLE doctor_visit_statistics ( "
				+ "        id integer, "
				+ "        institution_name varchar(500), "
				+ "        doctor_fio varchar(250), "
				+ "        speciality_profile varchar(50), "
				+ "        sector_number varchar(10), "
				+ "        first_date date, "
				+ "        CONSTRAINT doctor_visit_statistics_pk PRIMARY KEY (id) "
				+ "); ";
		try {
			Statement st = cn.createStatement();
			st.execute(sqlCreateHeader);
		} catch (SQLException e) {
		}
		String sqlCreateBody = "CREATE TABLE doctor_visit_day " + "( "
				+ "    doctor_visit_statistics_id integer, " + "    day date, "
				+ "    first_ill_in_polyclinic integer, "
				+ "    second_ill_in_polyclinic integer, "
				+ "    medical_examination integer, "
				+ "    medical_examination_less_year_in_polyclinic integer, "
				+ "    medical_examination_dn_in_polyclinic integer, "
				+ "    medical_examination_teenager_in_polyclinic integer, "
				+ "    first_ill_at_home integer, "
				+ "    second_ill_at_home integer, "
				+ "    active_at_home integer, "
				+ "    patronage_at_home integer, "
				+ "    less_year_at_home integer, "
				+ "    teenager_at_home integer, "
				+ "    CONSTRAINT doctor_visit_statistics_id_fk "
				+ "           FOREIGN KEY (doctor_visit_statistics_id) "
				+ "           REFERENCES doctor_visit_statistics (id) "
				+ "           ON DELETE CASCADE "
				+ "           ON UPDATE CASCADE " + "); ";
		try {
			Statement st = cn.createStatement();
			st.execute(sqlCreateBody);
		} catch (SQLException e) {
		}
	}

	public void dispose() {
		try {
			Statement st = cn.createStatement();
			st.execute("DELETE FROM DOCTOR_VISIT_STATISTICS WHERE ID <"
					+ (findMaxId() - 20));
			st.execute("SHUTDOWN");
		} catch (SQLException e) {
		}
		try {
			if (cn != null && !cn.isClosed()) {
				cn.close();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void save(DoctorVisitStatistics dvs) {
		try {
			if (cn == null || cn.isClosed()) {
				throw new IllegalStateException();  
			}
			int nextId = findMaxId() + 1;
			
			String sqlInsertHeader = "INSERT INTO DOCTOR_VISIT_STATISTICS (" +
					"ID," +
					"INSTITUTION_NAME," +
					"DOCTOR_FIO," +
					"SPECIALITY_PROFILE," +
					"SECTOR_NUMBER," +
					"FIRST_DATE) " +
					"VALUES " +
					"(?,?,?,?,?,?)";
			

	}}