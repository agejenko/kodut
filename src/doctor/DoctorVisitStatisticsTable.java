package doctor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class DoctorVisitStatisticsTable {

	private final static String[] COL_RESOURCE_NAMES = new String[] { "day",
			"firstIllInPolyclinic", "secondIllInPolyclinic",
			"medicalExamination", "medicalExaminationLessYearInPolyclinic",
			"medicalExaminationDNInPolyclinic",
			"medicalExaminationTeenagerInPolyclinic", "firstIllAtHome",
			"secondIllAtHome", "activeAtHome", "patronageAtHome",
			"lessYearAtHome", "teenagerAtHome" };

	private static final DateFormat DAY_FORMAT = new SimpleDateFormat("dd.MM");

	private Table table;
	private TableViewer tableViewer;

}