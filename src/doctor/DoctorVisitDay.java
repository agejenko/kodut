package doctor;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DoctorVisitDay {

	private Integer firstIllInPolyclinic = null;
	private Integer secondIllInPolyclinic = null;
	private Integer medicalExamination = null;
	private Integer medicalExaminationLessYearInPolyclinic = null;
	private Integer medicalExaminationDNInPolyclinic = null;
	private Integer medicalExaminationTeenagerInPolyclinic = null;

	private Integer firstIllAtHome = null;
	private Integer secondIllAtHome = null;
	private Integer activeAtHome = null;
	private Integer patronageAtHome = null;
	private Integer lessYearAtHome = null;
	private Integer teenagerAtHome = null;

	public DoctorVisitDay(Date date) {
		super();
		if (date == null) {
			throw new IllegalArgumentException();
		}
		this.date = date;
	}

	public Integer getDay() {
		GregorianCalendar g = new GregorianCalendar();
		g.setTime(date);
		return g.get(Calendar.DAY_OF_MONTH);
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		if (date == null) {
			throw new IllegalArgumentException();
		}
		this.date = date;
	}

	public Integer getFirstIllInPolyclinic() {
		return firstIllInPolyclinic;
	}

	public void setFirstIllInPolyclinic(Integer firstIllInPolyclinic) {
		this.firstIllInPolyclinic = nz(firstIllInPolyclinic);
	}

	public Integer getSecondIllInPolyclinic() {
		return secondIllInPolyclinic;
	}

	public void setSecondIllInPolyclinic(Integer secondIllInPolyclinic) {
		this.secondIllInPolyclinic = nz(secondIllInPolyclinic);
	}

	public Integer getMedicalExamination() {
		return medicalExamination;
	}

	public void setMedicalExamination(Integer medicalExamination) {
		this.medicalExamination = nz(medicalExamination);
	}

	public Integer getMedicalExaminationLessYearInPolyclinic() {
		return medicalExaminationLessYearInPolyclinic;
	}

	public void setMedicalExaminationLessYearInPolyclinic(
			Integer medicalExaminationLessYearInPolyclinic) {
		this.medicalExaminationLessYearInPolyclinic = nz(
				medicalExaminationLessYearInPolyclinic);
	}

	public Integer getMedicalExaminationDNInPolyclinic() {
		return medicalExaminationDNInPolyclinic;
	}

	public void setMedicalExaminationDNInPolyclinic(
			Integer medicalExaminationDNInPolyclinic) {
		this.medicalExaminationDNInPolyclinic = nz(
				medicalExaminationDNInPolyclinic);
	}

	public Integer getMedicalExaminationTeenagerInPolyclinic() {
		return medicalExaminationTeenagerInPolyclinic;
	}

	public void setMedicalExaminationTeenagerInPolyclinic(
			Integer medicalExaminationTeenagerInPolyclinic) {
		this.medicalExaminationTeenagerInPolyclinic = nz(
				medicalExaminationTeenagerInPolyclinic);
	}

	public Integer getFirstIllAtHome() {
		return firstIllAtHome;
	}

	public void setFirstIllAtHome(Integer firstIllAtHome) {
		this.firstIllAtHome = nz(firstIllAtHome);
	}

	public Integer getSecondIllAtHome() {
		return secondIllAtHome;
	}

	public void setSecondIllAtHome(Integer secondIllAtHome) {
		this.secondIllAtHome = nz(secondIllAtHome);
	}

	public Integer getActiveAtHome() {
		return activeAtHome;
	}

	public void setActiveAtHome(Integer activeAtHome) {
		this.activeAtHome = nz(activeAtHome);
	}

	public Integer getPatronageAtHome() {
		return patronageAtHome;
	}

	public void setPatronageAtHome(Integer patronageAtHome) {
		this.patronageAtHome = nz(patronageAtHome);
	}

	public Integer getLessYearAtHome() {
		return lessYearAtHome;
	}

	public void setLessYearAtHome(Integer lessYearAtHome) {
		this.lessYearAtHome = nz(lessYearAtHome);
	}

	public Integer getTeenagerAtHome() {
		return teenagerAtHome;
	}

	public void setTeenagerAtHome(Integer teenagerAtHome) {
		this.teenagerAtHome = nz(teenagerAtHome);
	}

	private Integer nz(Integer value) {
		if (value != null && value == 0) {
			return null;
		} else {
			return value;
		}
	}
}
