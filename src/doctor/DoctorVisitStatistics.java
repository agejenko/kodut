package doctor;

import java.beans.PropertyChangeSupport;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class DoctorVisitStatistics {
	public final static int FIRST_DAY = 2;

	protected PropertyChangeSupport propertyChangeSupport;

	private String specialityProfile;
	private String sectorNumber;
	private int month;
	private int year;
	private List<DoctorVisitDay> visits;

	public DoctorVisitStatistics() {
		super();
		propertyChangeSupport = new PropertyChangeSupport(this);

		Calendar date = new GregorianCalendar();

		month = date.get(Calendar.MONTH);
		year = date.get(Calendar.YEAR);

		initializeVisits();
	}

	private void initializeVisits() {
		visits = new ArrayList<DoctorVisitDay>();

		int i = 0;
		boolean ok = true;
		do {
			GregorianCalendar day = new GregorianCalendar(year, month,
					FIRST_DAY);
			day.add(Calendar.MONTH, -1);
			day.add(Calendar.DAY_OF_MONTH, i);
			DoctorVisitDay dvd = new DoctorVisitDay(day.getTime());
			visits.add(dvd);
			i++;
			day.add(Calendar.DAY_OF_MONTH, 1);
			if (day.get(Calendar.DAY_OF_MONTH) == FIRST_DAY) {
				ok = false;
			}
		} while (ok);
	}

	public String getSpecialityProfile() {
		return specialityProfile;
	}

	public String getSectorNumber() {
		return sectorNumber;
	}

	public int getMonth() {
		return month;
	}

	public String getMonthName() {
		GregorianCalendar d = new GregorianCalendar(year, month, 1);
		SimpleDateFormat df = new SimpleDateFormat("MMMMM");
		String monthName = df.format(d.getTime());
		return monthName;
	}

	public void setMonth(String monthName) {
		String oldName = getMonthName();
		
		Integer newValue;
		try {
			SimpleDateFormat df = new SimpleDateFormat("MMMMM");
			Date date = df.parse(monthName);
			GregorianCalendar g = new GregorianCalendar();
			g.setTime(date);
			newValue = g.get(Calendar.MONTH);
		} catch (ParseException e) {
			newValue = null;
		}

	public int getYear() {
		return year;
	}

	public List<DoctorVisitDay> getVisitDays() {
		return Collections.unmodifiableList(visits);
	}

}
