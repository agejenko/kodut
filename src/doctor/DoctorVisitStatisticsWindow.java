package doctor;

import java.beans.PropertyChangeListener;
import java.util.ResourceBundle;

import org.eclipse.jface.window.ApplicationWindow;

public class DoctorVisitStatisticsWindow extends ApplicationWindow
		implements PropertyChangeListener {

	final ResourceBundle bundle;
	final DoctorVisitStatisticsDao dao;
	private DoctorVisitStatistics subject;

	private Text instituteNameValue;
	private Text doctorFioValue;
	private Text specialityProfileValue;
	private Text monthValue;
	private Text yearValue;
	private Text sectorNumberValue;
}